import java.io.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommentParser {

    private BufferedReader reader;
    private StringBuilder comments;
    private StringBuilder fullText;
    private ArrayList<String> commentsList;
    Pattern pattern;
    Matcher matcher;


    public CommentParser(String path) throws FileNotFoundException, IOException {
        reader = new BufferedReader(
                        new FileReader(
                            new File(path)));
        fullText = new StringBuilder();
        comments = new StringBuilder();
        commentsList = new ArrayList<>();
        getFullText();
    }

    public void showComments() {
        pattern = Pattern.compile("/\\*.+?\\*/|//.+ ");
        matcher = pattern.matcher(fullText);
        while (matcher.find()) {
            System.out.println(fullText.substring(matcher.start(), matcher.end()));
        }
    }

    private void getFullText() throws IOException {
        String st;
        while ((st = reader.readLine()) != null) {
            fullText.append(st);
        }
    }

    public String getText() {

        boolean isComment = false;

        for(int i = 0; i < fullText.length(); i++) {

            if(fullText.charAt(i) == '/' && fullText.charAt(i+1) == '*') {
                isComment = true;
            } else if (fullText.charAt(i) == '*' && fullText.charAt(i+1) == '/') {
                isComment = false;
            }

            if (isComment && (fullText.charAt(i) != '/')) {
                comments.append(fullText.charAt(i));
            }
        }

        return comments.toString(); // USE SUBSTRING
    }



    public static void main(String[] args) throws IOException {

        CommentParser parser = new CommentParser("/home/dmytro/IdeaProjects/" +
                "task10_IO_NIO/src/main/java/file.txt");

        parser.showComments();

    }
}
