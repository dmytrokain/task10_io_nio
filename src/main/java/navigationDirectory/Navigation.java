package navigationDirectory;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Navigation {

    private Path dir;
    private String path;
    private Scanner scanner;

    public Navigation(String path) {
        this.path = path;
        dir = Paths.get(path);
        scanner = new Scanner(System.in);
    }

    private void cd(String folder) {
        dir = Paths.get(path+"/" + folder);
        if(Files.exists(dir) && dir.getRoot() != null) {
            System.out.println("Moved to " + dir.getFileName());
        } else {
            System.out.println("Isn't folder or it's not exist");
            dir = Paths.get(path);                                  // if not exist - return stay on current
        }
    }

    private void ls() {
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
            for (Path file : stream) {
                System.out.println(file.getFileName());
            }
        } catch (IOException e) {}
    }

    private void showCurrentPath() {
        System.out.print("\n" + dir + "$ ");
    }

    public void runShell() {
        while (true) {
            showCurrentPath();
            String [] commandLine = scanner.nextLine().split(" ");
            if(commandLine[0].equals("cd")) {
                cd(commandLine[1]);
            } else if(commandLine[0].equals("ls")) {
                ls();
            } else if(commandLine[0].equals("exit")) {
                break;
            } else {
                System.out.println("wrong command, try again. To exit type \"exit\"");
                break;
            }
        }
    }

    public static void main(String[] args) {
        Navigation navigation = new Navigation("/home/dmytro/");
        navigation.runShell();
    }


}
